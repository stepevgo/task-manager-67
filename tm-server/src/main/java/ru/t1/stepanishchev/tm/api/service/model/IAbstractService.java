package ru.t1.stepanishchev.tm.api.service.model;

import org.springframework.stereotype.Service;
import ru.t1.stepanishchev.tm.model.AbstractModel;

@Service
public interface IAbstractService<M extends AbstractModel> {
}
