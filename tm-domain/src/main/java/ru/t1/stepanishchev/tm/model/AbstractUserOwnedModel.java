package ru.t1.stepanishchev.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @Column(nullable = true, name = "created")
    private Date created = new Date();

}